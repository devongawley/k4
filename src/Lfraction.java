import java.util.*;

/** This class represents fractions of form n/d where n and d are long integer 
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
      System.out.println(valueOf("2/4"));
      System.out.println(valueOf("2/4/"));
      System.out.println(valueOf("2/x"));
      System.out.println(valueOf("2/4/6"));
   }

   private long n;
   private long d;

   private static long gcd(long n, long d) { // Viide https://stackoverflow.com/questions/6618994/simplifying-fractions-in-java
      return d == 0 ? Math.abs(n) : gcd(d, n % d);
   }

   /** Constructor.
    * @param n numerator
    * @param d denominator > 0
    */
   public Lfraction (long n, long d) {
      if (d != 0) {
         long gcd = gcd(n, d);
         this.n = n / gcd;
         this.d = d / gcd;

         if (this.d < 0) {
            this.n = -this.n;
            this.d = -this.d;
         }
      } else {
         throw new RuntimeException(String.format("Inputted denominator '%s' needs to be larger than 0.", d));
      }
   }

   /** Public method to access the numerator field.
    * @return numerator
    */
   public long getNumerator() {
      return n;
   }

   /** Public method to access the denominator field.
    * @return denominator
    */
   public long getDenominator() {
      return d;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      String stringRepresentation = getNumerator() + "/" + getDenominator();;
      return stringRepresentation;
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      return compareTo((Lfraction) m) == 0;

   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Objects.hash(this.n, this.d);
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      return m.minus(opposite());
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times(Lfraction m) {
      Lfraction lfraction = new Lfraction(this.n * m.n, this.d * m.d);
      return lfraction;
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if (this.n == 0) {
         throw new IllegalArgumentException("By inverting this fraction the numerator '0' becomes the denominator. You cannot divide by 0");
      }
      if (this.n < 1) {
         return new Lfraction(-this.d, -this.n);
      } else {
         return new Lfraction(this.d, this.n);
      }
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(-this.n, this.d);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus(Lfraction m) {
      if (this.d == m.d) {
         return new Lfraction(this.n - m.n, this.d);
      } else {
         Lfraction lfraction = new Lfraction((this.n * m.d) - (m.n * this.d), (m.d * this.d));
         return lfraction;
      }
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      if (m.n * m.d == 0) {
         throw new IllegalArgumentException(String.format("Fraction %s is equal to zero of infinity. You cannot divide by either.", m.toString()));
      }
      long n = Math.abs(this.n * m.d);
      long d = Math.abs(this.d * m.n);
      return new Lfraction(n, d);
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      return Long.compare(this.n * m.d, m.n * this.d);
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      try {
         return new Lfraction(this.n, this.d);
      } catch (Exception e) {
         throw new CloneNotSupportedException("Clone not supported");
      }
   }

   /** Integer part of the (improper) fraction.
    * @return integer part of this fraction
    */
   public long integerPart() {
      return this.n / this.d;
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      if (Math.abs(this.n) >= Math.abs(this.d)) {
         return new Lfraction(this.n % this.d, this.d);
      } else {
         return new Lfraction(this.n, this.d);
      }
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return (double) this.n / this.d;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      return new Lfraction(Math.round(f * d), d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      try {
         String[] fractionAsStringArray = s.split("/");
         if (fractionAsStringArray.length > 2) {
            throw new RuntimeException(String.format("Inputted string %s does not follow the correct format of a fraction (a/b).", s));
         }
         return new Lfraction(Long.parseLong(fractionAsStringArray[0]), Long.parseLong(fractionAsStringArray[1]));
      } catch (Exception e) {
         throw new RuntimeException(String.format("Inputted string %s does not follow the correct format of a fraction (a/b).", s));
      }
   }
}

